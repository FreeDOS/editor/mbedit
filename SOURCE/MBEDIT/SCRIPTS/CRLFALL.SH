
# crlfall.sh
# Convert all *.c- and *.h-files in a whole directory tree from <cr><lf> to <lf>.

find . -name "*.c"       -exec crlf2lf.sh {} \;
find . -name "*.cpp"     -exec crlf2lf.sh {} \;
find . -name "*.h"       -exec crlf2lf.sh {} \;
find . -name "*.hh"      -exec crlf2lf.sh {} \;
find . -name "makefile"  -exec crlf2lf.sh {} \;
find . -name "*.mk"      -exec crlf2lf.sh {} \;
find . -name "*.sh"      -exec crlf2lf.sh {} \;
find . -name "*.bat"     -exec crlf2lf.sh {} \;
find . -name "*.tcl"     -exec crlf2lf.sh {} \;
find . -name "*.cfg"     -exec crlf2lf.sh {} \;
find . -name "*.txt"     -exec crlf2lf.sh {} \;
